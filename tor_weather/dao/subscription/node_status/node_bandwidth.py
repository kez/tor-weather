from typing import Any

from tor_weather.constants import SubscriptionType
from tor_weather.core.database.tables import NodeBandwidthSub
from tor_weather.dao.subscription.subscription_dao import SubscriptionDao

# DISCUSSION:
# 1.  A user can have a single subscription with a fingerprint. This means, a single user_id & a
#     single relay_id will only have a single subscription.
#
# 2.  That single global subscription can have a max of any of the available subscriptions attached
#     to it.
#
# 3. When creating new subscription, we will check if user already has a subscription created for
#    that relay. If he already has one, we add create a child subscription & link with the global
#    one.

# 4. Else, incase the user does not have a subscription with the relay, we create a new global
#    subscription and then, create a new child subscription which is then linked with the global
#    subscription.
#
# 5. When modifying a subscription, we will check if the user already has a subscription. If he has,
#    we will modify it, else, throw an error
#
# 6. When deleting a subscription, we will check if the user already has a subscription. If he has,
#    we will delete it, else, throw an error.


class NodeBandwidthDao(SubscriptionDao):
    def __init__(self, email: str, flag=None):
        super().__init__(email)
        self.redirect_url = "/dashboard/node-status/node-bandwidth"
        self.subscription_type = SubscriptionType["node-bandwidth"]

    def _create_current_sub(self, data: Any) -> NodeBandwidthSub:
        """Create a current subscription object"""
        subscription: NodeBandwidthSub = NodeBandwidthSub(
            wait_for=data.get("wait_for"), threshold=data.get("threshold")
        )
        return subscription

    def _modify_current_sub(self, child_sub: NodeBandwidthSub, data: Any) -> None:
        """Modify the current subscription object"""
        child_sub.wait_for = data.get("wait_for")
        child_sub.threshold = data.get("threshold")
