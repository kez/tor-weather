from typing import Any


class Email:
    def __init__(self, receiver: str) -> None:
        self.receiver: str = receiver
        self.email_subject: str = None  # type: ignore
        self.email_body: str = None  # type: ignore

    def _create_body(self, data: Any) -> str:  # type: ignore
        pass

    def _create_subject(self, data: Any) -> str:  # type: ignore
        pass
