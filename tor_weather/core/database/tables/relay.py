from tor_weather.extensions import db


class Relay(db.Model):
    id = db.Column(db.Integer, primary_key=True, nullable=False)
    is_exit = db.Column(db.Boolean, nullable=False, default=False)
    fingerprint = db.Column(db.String(40), nullable=False, unique=True)
    first_seen = db.Column(db.DateTime, nullable=False)
    last_seen = db.Column(db.DateTime, nullable=False)
    version = db.Column(db.String(32), nullable=False)
    recommended_version = db.Column(db.Boolean, nullable=False, default=False)
    is_up = db.Column(db.Boolean, nullable=False, default=False)
    subscriptions = db.relationship(
        "Subscription", foreign_keys="Subscription.relay_id", backref="relay", lazy=True
    )
    hosts = db.relationship(
        "Subscription", foreign_keys="Subscription.host_id", backref="host", lazy=True
    )
