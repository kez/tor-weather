from typing import Any, Literal

from tor_weather.utilities import kebab_to_title

sub_form_data: dict[str, dict[str, str]] = {
    "fingerprint": {
        "display_name": "Fingerprint",
        "input_type": "text",
    },
    "wait_for": {
        "display_name": "Wait For (hrs)",
        "input_type": "number",
    },
    "threshold": {
        "display_name": "Threshold Bandwidth",
        "input_type": "number",
    },
}


class SubscriptionForm:
    """Data for Subscription Form to be displayed in the template for creating & modifying subscriptions"""

    def __init__(self, subscription_category: str, subscription_name: str) -> None:
        self.subscription_category: str = subscription_category
        self.subscription_name: str = subscription_name

    def _get_header(self, isCreate: bool) -> str:
        """Get text to be shown in the header of subscription form"""
        route_type: Literal["Create", "Modify"] = "Create" if isCreate else "Modify"
        sub_name_title: str = kebab_to_title(self.subscription_name)
        sub_category_title: str = (
            kebab_to_title(self.subscription_category)
            if self.subscription_category == "node-flag"
            else ""
        )
        return f"{route_type} {sub_category_title} - {sub_name_title} Subscription"

    def _get_api_route(self, isCreate: bool) -> str:
        """Get api route for form-submission"""
        route_type: Literal["create", "modify"] = "create" if isCreate else "modify"
        return f"/api/dashboard/{self.subscription_category}/{self.subscription_name}/{route_type}"

    def _get_form_fields(self, form_fields: list[str], isCreate: bool, data=None):
        """Get a list of form-fields object for the form to be rendered in the template"""
        form_fields_data = []
        for field in form_fields:
            field_data: dict[str, str] = sub_form_data[field]
            form_fields_data.append(
                {
                    "value": field,
                    "input_type": field_data["input_type"],
                    "display_name": field_data["display_name"],
                    "disabled": field == "fingerprint" and not isCreate,
                    "default_value": data.get(field, "") if data else "",  # type: ignore
                }
            )
        return form_fields_data

    def _get_data(self, form_fields: list[str], isCreate: bool, data=None):
        """Get data for the form-fields"""
        return {
            "header": self._get_header(isCreate),
            "endpoint": self._get_api_route(isCreate),
            "button": "Create" if isCreate else "Modify",
            "fields": self._get_form_fields(form_fields, isCreate, data),
        }

    def create_sub(self, form_fields: list[str]):
        """Get data for form-fields for creating subscriptions"""
        return self._get_data(form_fields, True)

    def modify_sub(self, form_fields: list[str], data: Any):
        """Get data for form-fields for modifying subscriptions"""
        return self._get_data(form_fields, False, data)
