from typing import Type

from tor_weather.dao.subscription.node_status.node_down import NodeDownDao
from tor_weather.service.subscription_wrapper import SubscriptionService


class NodeDownService(SubscriptionService):

    TABLE_HEADER_KEYS: list[str] = ["fingerprint", "wait_for", "is_active"]
    TABLE_DAO: Type[NodeDownDao] = NodeDownDao  # type: ignore

    def __init__(self, email: str) -> None:
        super().__init__(email)

    def _get_modify_url(self, id: int) -> str:
        return f"/dashboard/node-status/node-down/{id}/modify"

    def _get_delete_url(self, id: str) -> str:
        return f"/api/dashboard/node-status/node-down/{id}/delete"

    def _get_enable_url(self, id: str) -> str:
        return f"/api/dashboard/node-status/node-down/{id}/enable"

    def _get_disable_url(self, id: str) -> str:
        return f"/api/dashboard/node-status/node-down/{id}/disable"

    def get_subscriptions(self):
        """Get all the Node Down Subscriptions for the user"""
        return {
            "header": super()._get_table_header(),
            "content": super()._get_table_content(),
        }

    def get_subscription_data(self, fingerprint: str):
        """Get data for Node Down Subscription for the user"""
        return self.TABLE_DAO(email=self.email).get_subscription_data(fingerprint)

    def create_subscription(self, fingerprint: str, wait_for: int):
        """Create a Node Down Subscription"""
        data = {"fingerprint": fingerprint, "wait_for": wait_for}
        return self.TABLE_DAO(email=self.email).create_subscription(data)

    def modify_subscription(self, fingerprint: str, wait_for: int):
        """Modify a Node Down Subscription"""
        data = {"fingerprint": fingerprint, "wait_for": wait_for}
        return self.TABLE_DAO(email=self.email).modify_subscription(data)

    def deactivate_subscription(self, fingerprint: str):
        """Deactivate a Node Down Subscription"""
        data = {"fingerprint": fingerprint}
        return self.TABLE_DAO(email=self.email).deactivate_subscription(data)

    def activate_subscription(self, fingerprint: str):
        """Activate a Node Down Subscription"""
        data = {"fingerprint": fingerprint}
        return self.TABLE_DAO(email=self.email).activate_subscription(data)

    def delete_subscription(self, fingerprint: str):
        """Delete a Node Down Subscription"""
        data = {"fingerprint": fingerprint}
        return self.TABLE_DAO(email=self.email).delete_subscription(data)
