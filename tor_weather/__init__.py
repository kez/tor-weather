from typing import Literal

from flask import Flask, redirect, render_template
from flask_assets import Bundle, Environment
from flask_login import LoginManager
from flask_restx import Api
from werkzeug import Response

from tor_weather.error import APIError

from .routes import configure_namespaces, register_routes


# Creates an entry for the overall project
def create_app() -> Flask:
    """Creates & Intialises the Flask Backend

    Returns:
        Flask: Instance of Flask App
    """
    # 1. Create an app
    app: Flask = _get_app()
    # 2. Add config for environment variables
    configure_env_variables(app)
    # 3. Configure the secret key for the app
    configure_app_secret(app)
    # 4. Configure the assets
    configure_assets(app)
    # 5. Configure the home route
    configure_home_route(app)
    # 6. Initialise the routes
    configure_routes(app)
    # 7. Configure the database
    configure_database(app)
    # 8. Configure the login manager
    configure_login_manager(app)
    # 9. Return the app
    return app


def entry_onionoo_job() -> None:
    """
    Creates an entry for executing the onionoo job
    """
    # 1. Create an app
    app: Flask = _get_app()
    # 2. Add config for environment variables
    configure_env_variables(app)
    # 3. Configure the secret key for the app
    configure_app_secret(app)
    # 4. Configure the database
    configure_database(app)
    # 5. Push the current app in the app_context
    app.app_context().push()
    # 6. Import the Onionoo Job
    from tor_weather.jobs.onionoo import onionoo_job

    # 7. Run the Onionoo Job
    onionoo_job()


def entry_add_mock() -> None:
    """
    Creates an entry for adding the mock data in the database
    """
    # 1. Create an app
    app: Flask = _get_app()
    # 2. Add config for environment variables
    configure_env_variables(app)
    # 3. Configure the secret key for the app
    configure_app_secret(app)
    # 4. Configure the database
    configure_database(app)
    # 5. Import the function for adding mock data
    import tor_weather.core.database

    # 6. Execute the function for adding mock data
    tor_weather.core.database.initiate_tables()


def _get_app() -> Flask:
    """Creates an app instance & returns it

    Returns:
        Flask: Instance of Flask App
    """
    app: Flask = Flask(__name__, template_folder="./templates")
    return app


def configure_database(app: Flask) -> None:
    """Configures the database instance with the tables

    Args:
        app (Flask): The current app instance
    """
    from .extensions import db

    db.init_app(app)

    # 1. Create the database tables
    with app.app_context():
        db.create_all()


def configure_env_variables(app: Flask) -> None:
    """Configures the environment variables for the application

    Args:
        app (Flask): The current app instance
    """
    # Import the function to store local copy of env variables
    from tor_weather.env import configure_env

    # Store the local copy of environment variables
    configure_env(app)


def configure_app_secret(app: Flask) -> None:
    """Configures the asset conversion extensions

    Args:
        app (Flask): The current app instance
    """
    app.secret_key = app.config["SECRET_KEY"]


def configure_assets(app: Flask) -> None:
    """Configures the asset conversion extensions

    Args:
        app (Flask): The current app instance
    """
    # 1. Configure the SCSS to CSS Conversion
    configure_scss(app)


def configure_scss(app: Flask) -> None:
    """Configures the conversion of SCSS to CSS

    Args:
        app (Flask): The current app instance
    """
    assets: Environment = Environment(app)
    assets.register(
        "main_scss",
        Bundle(
            "assets/styles/main.scss",
            filters="pyscss",
            depends="assets/styles/**/*.scss",
            output="styles/main.%(version)s.css",
        ),
    )


def configure_login_manager(app: Flask) -> None:
    """Configures the login manager

    Args:
        app (Flask): The current app instance
    """
    login_manager = LoginManager()
    login_manager.login_view = "login"  # type: ignore
    login_manager.init_app(app)

    from tor_weather.core.database.tables.subscriber import Subscriber

    @login_manager.user_loader
    def load_user(user_id):
        return Subscriber.query.get(int(user_id))


def configure_home_route(app: Flask) -> None:
    """Configures the home route for the app

    Args:
        app (Flask): The current app instance
    """

    @app.get("/")
    def home() -> Response:
        """Redirects the request from the home route to the login route

        Returns:
            Response: Redirects the user to login route
        """
        return redirect("/login")


def configure_routes(app: Flask) -> None:
    """Configures the routes

    Args:
        app (Flask): The current app instance
    """
    # 1. Create an instance for the restx api
    api: Api = Api(doc="/documentation")
    # 2. Initialise the Flask_RestX with the App
    api.init_app(app)
    # 3. Configure the namespaces
    configure_namespaces(api)
    # 4. Register the routes
    register_routes()
    # 5. Register the error handler routes
    register_error_routes(app, api)


def register_error_routes(app: Flask, api: Api) -> None:
    """
    Configures the error routes for the app
    """

    @api.errorhandler(APIError)  # type: ignore
    def handle_error(error):
        return {"message": error.message}, error.status_code

    @app.errorhandler(400)
    def bad_request(e) -> tuple[str, Literal[400]]:
        """Redirect the user to 400 Page incase of Bad Request

        Args:
            e (_type_): Error

        Returns:
            tuple[str, Literal[400]]: Template for 400 Route
        """
        return render_template("pages/error/400.html"), 400

    @app.errorhandler(401)
    def unauthorized(e) -> tuple[str, Literal[401]]:
        """Redirect the user to 401 Page incase of Unauthorized request

        Args:
            e (_type_): Error

        Returns:
            tuple[str, Literal[401]]: Template for 401 Route
        """
        return render_template("pages/error/401.html"), 401

    @app.errorhandler(404)
    def not_found(e) -> tuple[str, Literal[404]]:
        """Redirect the user to 404 Page incase of Page not found

        Args:
            e (_type_): Error

        Returns:
            tuple[str, Literal[404]]: Template for 404 Route
        """
        return render_template("pages/error/404.html"), 404

    @app.errorhandler(500)
    def server_error(e) -> tuple[str, Literal[500]]:
        """Redirect the user to 500 Page incase of Internal Server Error

        Args:
            e (_type_): Error

        Returns:
            tuple[str, Literal[500]]: Template for 500 Route
        """
        return render_template("pages/error/500.html"), 500
