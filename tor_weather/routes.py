from typing import Optional

from flask_restx import Api, Namespace

from tor_weather.constants import EXTERNAL_NS_DISC, TEMPLATE_NS_DISC

TEMPLATE_NS: Optional[Namespace] = None
API_NS: Optional[Namespace] = None


def template_ns() -> Namespace:
    """Get the Template Namespace

    Raises:
        Exception: If the Template Namespace is accessed before initialisation

    Returns:
        Namespace: Template Namespace
    """
    global TEMPLATE_NS
    if not TEMPLATE_NS:
        raise Exception("Template Namespace is not yet defined")
    else:
        return TEMPLATE_NS


def api_ns() -> Namespace:
    """Get the API Namespace

    Raises:
        Exception: If the API Namespace is accessed before initialisation

    Returns:
        Namespace: API Namespace
    """
    global API_NS
    if not API_NS:
        raise Exception("API Namespace is not yet defined")
    else:
        return API_NS


def configure_namespaces(api: Api) -> None:
    """Configures the namespaces

    Args:
        api (Api): Flask Restx API Instance
    """
    global TEMPLATE_NS, API_NS
    TEMPLATE_NS = Namespace("template_routes", description=TEMPLATE_NS_DISC)
    API_NS = Namespace("api_routes", description=EXTERNAL_NS_DISC)
    api.add_namespace(TEMPLATE_NS, path="/")
    api.add_namespace(API_NS, path="/api/")


def get_routes():
    # Imports for the Template Routes
    from tor_weather.resources.external_api_routes.email_verification import (
        EmailVerificationApi,
    )

    # Imports for the External Routes
    from tor_weather.resources.external_api_routes.login_user import LoginApi
    from tor_weather.resources.external_api_routes.logout_user import LogoutApi
    from tor_weather.resources.external_api_routes.register_user import RegisterApi
    from tor_weather.resources.external_api_routes.subscription_create.node_flag.node_flag import (
        NodeFlagSubscriptionCreateApi,
    )
    from tor_weather.resources.external_api_routes.subscription_create.node_status.node_bandwidth import (
        NodeBandwidthSubscriptionCreateApi,
    )
    from tor_weather.resources.external_api_routes.subscription_create.node_status.node_down import (
        NodeDownSubscriptionCreateApi,
    )
    from tor_weather.resources.external_api_routes.subscription_modify.node_flag.node_flag import (
        NodeFlagSubscriptionModifyApi,
    )
    from tor_weather.resources.external_api_routes.subscription_modify.node_status.node_bandwidth import (
        NodeBandwidthSubscriptionModifyApi,
    )
    from tor_weather.resources.external_api_routes.subscription_modify.node_status.node_down import (
        NodeDownSubscriptionModifyApi,
    )
    from tor_weather.resources.external_api_routes.subscription_status.activate.node_flag.node_flag import (
        NodeFlagSubscriptionActivateApi,
    )
    from tor_weather.resources.external_api_routes.subscription_status.activate.node_status.node_bandwidth import (
        NodeBandwidthSubscriptionActivateApi,
    )
    from tor_weather.resources.external_api_routes.subscription_status.activate.node_status.node_down import (
        NodeDownSubscriptionActivateApi,
    )
    from tor_weather.resources.external_api_routes.subscription_status.deactivate.node_flag.node_flag import (
        NodeFlagSubscriptionDeactivateApi,
    )
    from tor_weather.resources.external_api_routes.subscription_status.deactivate.node_status.node_bandwidth import (
        NodeBandwidthSubscriptionDeactivateApi,
    )
    from tor_weather.resources.external_api_routes.subscription_status.deactivate.node_status.node_down import (
        NodeDownSubscriptionDeactivateApi,
    )
    from tor_weather.resources.external_api_routes.subscription_status.delete.node_flag.node_flag import (
        NodeFlagSubscriptionDeleteApi,
    )
    from tor_weather.resources.external_api_routes.subscription_status.delete.node_status.node_bandwidth import (
        NodeBandwidthSubscriptionDeleteApi,
    )
    from tor_weather.resources.external_api_routes.subscription_status.delete.node_status.node_down import (
        NodeDownSubscriptionDeleteApi,
    )
    from tor_weather.resources.template_routes.about import AboutTemplate
    from tor_weather.resources.template_routes.dashboard.home import (
        DashboardHomeTemplate,
    )
    from tor_weather.resources.template_routes.dashboard.subscription_create.node_flag.node_flag import (
        NodeFlagCreateTemplate,
    )
    from tor_weather.resources.template_routes.dashboard.subscription_create.node_status.node_bandwidth import (
        NodeBandwidthCreateTemplate,
    )
    from tor_weather.resources.template_routes.dashboard.subscription_create.node_status.node_down import (
        NodeDownCreateTemplate,
    )
    from tor_weather.resources.template_routes.dashboard.subscription_list import (
        DashboardListTemplate,
    )
    from tor_weather.resources.template_routes.dashboard.subscription_modify.node_flag.node_flag import (
        NodeFlagModifyTemplate,
    )
    from tor_weather.resources.template_routes.dashboard.subscription_modify.node_status.node_bandwidth import (
        NodeBandwidthModifyTemplate,
    )
    from tor_weather.resources.template_routes.dashboard.subscription_modify.node_status.node_down import (
        NodeDownModifyTemplate,
    )
    from tor_weather.resources.template_routes.login import LoginTemplate
    from tor_weather.resources.template_routes.register import RegisterTemplate

    routes = {
        "template_routes": [
            {
                "resource": AboutTemplate,
                "endpoints": ["about"],
                "endpoint_name": "about",
            },
            {
                "resource": RegisterTemplate,
                "endpoints": ["register"],
                "endpoint_name": "register",
            },
            {
                "resource": LoginTemplate,
                "endpoints": ["login"],
                "endpoint_name": "login",
            },
            {
                "resource": DashboardHomeTemplate,
                "endpoints": ["dashboard"],
                "endpoint_name": "dashboard_home",
            },
            {
                "resource": NodeDownCreateTemplate,
                "endpoints": ["dashboard/node-status/node-down/create"],
                "endpoint_name": "node_down_create",
            },
            {
                "resource": NodeDownModifyTemplate,
                "endpoints": ["dashboard/node-status/node-down/<fingerprint>/modify"],
                "endpoint_name": "node_down_modify",
            },
            {
                "resource": NodeBandwidthCreateTemplate,
                "endpoints": ["dashboard/node-status/node-bandwidth/create"],
                "endpoint_name": "node_bandwidth_create",
            },
            {
                "resource": NodeBandwidthModifyTemplate,
                "endpoints": [
                    "dashboard/node-status/node-bandwidth/<fingerprint>/modify"
                ],
                "endpoint_name": "node_bandwidth_modify",
            },
            {
                "resource": NodeFlagCreateTemplate,
                "endpoints": ["dashboard/node-flag/<flag_name>/create"],
                "endpoint_name": "node_flag_create",
            },
            {
                "resource": NodeFlagModifyTemplate,
                "endpoints": ["dashboard/node-flag/<flag_name>/<fingerprint>/modify"],
                "endpoint_name": "node_flag_modify",
            },
            {
                "resource": DashboardListTemplate,
                "endpoints": ["dashboard/node-status/<subscription_name>"],
                "endpoint_name": "node_status_list",
            },
            {
                "resource": DashboardListTemplate,
                "endpoints": ["dashboard/node-flag/<subscription_name>"],
                "endpoint_name": "node_flag_list",
            },
        ],
        "api_routes": [
            {
                "resource": RegisterApi,
                "endpoints": ["register"],
            },
            {
                "resource": LoginApi,
                "endpoints": ["login"],
            },
            {
                "resource": LogoutApi,
                "endpoints": ["logout"],
            },
            {
                "resource": EmailVerificationApi,
                "endpoints": ["email-verification"],
            },
            # Node Down Subscription Endpoints
            {
                "resource": NodeDownSubscriptionCreateApi,
                "endpoints": ["dashboard/node-status/node-down/create"],
            },
            {
                "resource": NodeDownSubscriptionModifyApi,
                "endpoints": ["dashboard/node-status/node-down/modify"],
            },
            {
                "resource": NodeDownSubscriptionActivateApi,
                "endpoints": ["dashboard/node-status/node-down/<fingerprint>/activate"],
            },
            {
                "resource": NodeDownSubscriptionDeactivateApi,
                "endpoints": [
                    "dashboard/node-status/node-down/<fingerprint>/deactivate"
                ],
            },
            {
                "resource": NodeDownSubscriptionDeleteApi,
                "endpoints": ["dashboard/node-status/node-down/<fingerprint>/delete"],
            },
            # Node Bandwidth Subscription Endpoints
            {
                "resource": NodeBandwidthSubscriptionCreateApi,
                "endpoints": ["dashboard/node-status/node-bandwidth/create"],
            },
            {
                "resource": NodeBandwidthSubscriptionModifyApi,
                "endpoints": ["dashboard/node-status/node-bandwidth/modify"],
            },
            {
                "resource": NodeBandwidthSubscriptionActivateApi,
                "endpoints": [
                    "dashboard/node-status/node-bandwidth/<fingerprint>/activate"
                ],
            },
            {
                "resource": NodeBandwidthSubscriptionDeactivateApi,
                "endpoints": [
                    "dashboard/node-status/node-bandwidth/<fingerprint>/deactivate"
                ],
            },
            {
                "resource": NodeBandwidthSubscriptionDeleteApi,
                "endpoints": [
                    "dashboard/node-status/node-bandwidth/<fingerprint>/delete"
                ],
            },
            # Node Flag Subscription Endpoints
            {
                "resource": NodeFlagSubscriptionCreateApi,
                "endpoints": ["dashboard/node-flag/<flag_name>/create"],
            },
            {
                "resource": NodeFlagSubscriptionModifyApi,
                "endpoints": ["dashboard/node-flag/<flag_name>/modify"],
            },
            {
                "resource": NodeFlagSubscriptionActivateApi,
                "endpoints": ["dashboard/node-flag/<flag_name>/<fingerprint>/activate"],
            },
            {
                "resource": NodeFlagSubscriptionDeactivateApi,
                "endpoints": [
                    "dashboard/node-flag/<flag_name>/<fingerprint>/deactivate"
                ],
            },
            {
                "resource": NodeFlagSubscriptionDeleteApi,
                "endpoints": ["dashboard/node-flag/<flag_name>/<fingerprint>/delete"],
            },
        ],
    }
    return routes


def add_routes(route_type: Namespace) -> None:
    """
    Register routes for a particular route type
    """
    resources = get_routes()[route_type.name]
    for resource in resources:
        res_obj = resource.get("resource")
        res_endpoints = resource.get("endpoints")
        endpoint = resource.get("endpoint_name")
        if endpoint:
            route_type.add_resource(res_obj, *res_endpoints, endpoint=endpoint)
        else:
            route_type.add_resource(res_obj, *res_endpoints)


def register_routes() -> None:
    """Registers routes under the available namespaces"""
    add_routes(template_ns())
    add_routes(api_ns())
