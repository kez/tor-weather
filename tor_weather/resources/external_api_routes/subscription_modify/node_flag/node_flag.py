from typing import cast

from flask_login import current_user, login_required
from flask_restx import Resource

from tor_weather.core.database.tables.subscriber import Subscriber
from tor_weather.extensions import backend_logger
from tor_weather.routes import api_ns
from tor_weather.service.subscription.node_flag import NodeFlagService

request_parser = api_ns().parser()
request_parser.add_argument(
    "fingerprint",
    type=str,
    help="Fingerprint of the node to be modified",
    required=True,
    location="form",
)
request_parser.add_argument(
    "wait_for",
    type=int,
    help="Time to wait before sending an email",
    required=True,
    location="form",
)
# request_parser.add_argument(
#     "flag_name",
#     choices=("exit", "fast", "guard", "stable", "valid"),
#     help="Flag for which the list of subscriptions needs to be fetched",
#     required=True,
# )


class NodeFlagSubscriptionModifyApi(Resource):
    """Implements the Node Flag Subscription Modify API"""

    @login_required
    @api_ns().doc(
        parser=request_parser,
        description="API for modifying a node-flag subscription",
    )
    def post(self, flag_name: str):
        backend_logger.info("Node Flag Modify API Requested")
        # Get the subscriber from the session cookie
        subscriber = cast(Subscriber, current_user)
        args = request_parser.parse_args()
        return NodeFlagService(
            email=subscriber.email, flag=flag_name
        ).modify_subscription(
            fingerprint=args["fingerprint"],
            wait_for=args["wait_for"],
        )
