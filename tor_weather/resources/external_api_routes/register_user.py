from flask import Response
from flask_restx import Resource

from tor_weather.routes import api_ns
from tor_weather.service.user import User

# Creating a parser for the api route
register_parser = api_ns().parser()
register_parser.add_argument(
    "email", type=str, help="Email ID of the User", location="form", required=True
)
register_parser.add_argument(
    "password", type=str, help="Password of the User", location="form", required=True
)


# Create a resource class for the api route
class RegisterApi(Resource):
    """Implements all API's related to Register"""

    @api_ns().doc(
        parser=register_parser,
        description="API for User Registration for the Tor-Weather Portal",
        responses={
            302: "Redirect to Login Page if successful or user already exists",
        },
    )
    def post(self) -> Response:
        args = register_parser.parse_args()
        email = args["email"]
        password = args["password"]
        return User().register(email, password)
