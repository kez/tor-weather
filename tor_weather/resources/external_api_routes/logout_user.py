from flask import Response, make_response, redirect
from flask_login import logout_user
from flask_restx import Resource

from tor_weather.extensions import backend_logger
from tor_weather.routes import api_ns


# Create a resource class for the api route
class LogoutApi(Resource):
    """Implements all API's related to Logout"""

    @api_ns().doc(
        description="API for Logging out of the dashboard.",
        responses={
            302: "Redirect to Login Page",
        },
    )
    def get(self) -> Response:
        backend_logger.info("Logout API Requested")
        # 1. Clear the user's session cookie
        logout_user()
        # 2. Redirect the user to the home page
        return make_response(redirect("/"))
