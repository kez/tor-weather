from flask import request
from flask.wrappers import Response
from flask_restx import Resource

from tor_weather.routes import api_ns
from tor_weather.service.user import User


# Create a resource class for the api route
class EmailVerificationApi(Resource):
    """Implements all API's related to Email Confirmation"""

    @api_ns().doc(
        description="API for Email Confirmation into the Tor-Weather Portal",
        responses={
            302: "Redirect to Login or Incorrect Credentials",
        },
    )
    def get(self) -> Response:
        verification_code: str = request.args["verification_code"]
        return User().verify_email(verification_code)
