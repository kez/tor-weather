from flask.wrappers import Response
from flask_restx import Resource

from tor_weather.routes import api_ns
from tor_weather.service.user import User

# Creating a parser for the api route
login_parser = api_ns().parser()
login_parser.add_argument(
    "email", type=str, help="Email ID of the User", location="form", required=True
)
login_parser.add_argument(
    "password", type=str, help="Password of the User", location="form", required=True
)


# Create a resource class for the api route
class LoginApi(Resource):
    """Implements all API's related to Login"""

    @api_ns().doc(
        parser=login_parser,
        description="API for User Login into the Tor-Weather Portal",
        responses={
            302: "Redirect to Dashboard or Incorrect Credentials",
        },
    )
    def post(self) -> Response:
        args = login_parser.parse_args()
        email = args["email"]
        password = args["password"]
        return User().login(email, password)
