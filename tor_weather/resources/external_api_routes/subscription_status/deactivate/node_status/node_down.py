from typing import cast

from flask_login import current_user, login_required
from flask_restx import Resource

from tor_weather.core.database.tables.subscriber import Subscriber
from tor_weather.extensions import backend_logger
from tor_weather.routes import api_ns
from tor_weather.service.subscription.node_down import NodeDownService


class NodeDownSubscriptionDeactivateApi(Resource):
    """Implements the Node Down Subscription Deactivate API"""

    @login_required
    @api_ns().doc(
        description="API for deactivating a node-down subscription",
    )
    def post(self, fingerprint: str):
        backend_logger.info("Node Down Deactivate API Requested")
        # Get the subscriber from the session cookie
        subscriber = cast(Subscriber, current_user)
        return NodeDownService(email=subscriber.email).deactivate_subscription(
            fingerprint
        )
