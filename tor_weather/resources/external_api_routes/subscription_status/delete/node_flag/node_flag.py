from typing import cast

from flask_login import current_user, login_required
from flask_restx import Resource

from tor_weather.core.database.tables.subscriber import Subscriber
from tor_weather.extensions import backend_logger
from tor_weather.routes import api_ns
from tor_weather.service.subscription.node_flag import NodeFlagService


class NodeFlagSubscriptionDeleteApi(Resource):
    """Implements the Node Flag Subscription Delete API"""

    @login_required
    @api_ns().doc(
        description="API for deleting a node-flag subscription",
    )
    def get(self, flag_name: str, fingerprint: str):
        backend_logger.info("Node Flag Delete API Requested")
        # Get the subscriber from the session cookie
        subscriber = cast(Subscriber, current_user)
        return NodeFlagService(
            email=subscriber.email, flag=flag_name
        ).delete_subscription(fingerprint)
