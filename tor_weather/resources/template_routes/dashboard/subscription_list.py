from typing import cast

from flask import make_response, render_template, request
from flask.wrappers import Response
from flask_login import current_user, login_required
from flask_restx import Resource

from tor_weather.core.database.tables.subscriber import Subscriber
from tor_weather.extensions import backend_logger
from tor_weather.routes import template_ns
from tor_weather.service.breadcrumb import Breadcrumb
from tor_weather.service.sidebar import Sidebar
from tor_weather.service.subscription.node_bandwidth import NodeBandwidthService
from tor_weather.service.subscription.node_down import NodeDownService
from tor_weather.service.subscription.node_flag import NodeFlagService

request_parser = template_ns().parser()
request_parser.add_argument(
    "email", type=str, help="Email ID of the User", required=True
)
request_parser.add_argument(
    "flag_name",
    choices=("exit", "fast", "guard", "stable", "valid"),
    help="Flag for which the list of subscriptions needs to be fetched",
    required=True,
)


class DashboardListTemplate(Resource):
    """Implements the Dashboard Subscription List Page"""

    def _get_table_data(self, email: str, subscription_name: str):
        if subscription_name == "node-down":
            return NodeDownService(email).get_subscriptions()
        elif subscription_name == "node-bandwidth":
            return NodeBandwidthService(email).get_subscriptions()
        else:
            return NodeFlagService(email, subscription_name).get_subscriptions()

    @login_required
    def get(self, subscription_name: str) -> Response:
        backend_logger.info("Subscription List Template Requested")
        # Get the subscriber from the session cookie
        subscriber = cast(Subscriber, current_user)
        # Get the content for the snackbar
        sidebar = Sidebar(request.path).get_data()
        breadcrumb = Breadcrumb(request.path).get_data()
        # Create a response with the template variables
        table_data = self._get_table_data(subscriber.email, subscription_name)
        create_url: str = f"{request.path}/create"
        return make_response(
            render_template(
                "/pages/dashboard/subscription-list.html",
                sidebar=sidebar,
                breadcrumb=breadcrumb,
                table=table_data,
                createUrl=create_url,
            )
        )
