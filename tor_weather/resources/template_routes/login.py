from flask import make_response, render_template
from flask.wrappers import Response
from flask_restx import Resource

from tor_weather.extensions import backend_logger
from tor_weather.service.header import Header as TemplateHeader


class LoginTemplate(Resource):
    """Implements the Login Page"""

    def get(self) -> Response:
        backend_logger.info("Login Template Requested")
        # Get the value of template variables for the route
        header = TemplateHeader("login").get_options()
        # Create a response with the template variables
        return make_response(render_template("pages/login.html", header=header))
