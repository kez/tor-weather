import enum
from typing import Literal

from tor_weather.models.constants_model import SubscriptionTypeInterface

HEADER_OPTIONS: dict[str, list[str]] = {
    "about": ["login", "register"],
    "login": ["register", "about"],
    "post_email_confirmation": ["login", "about"],
    "pre_email_confirmation": ["login", "about"],
    "register": ["login", "about"],
}

COOKIE_NAME: Literal["weatherKey"] = "weatherKey"

TABLE_HEADER_DISPLAY_NAME: dict[str, str] = {
    "fingerprint": "Fingerprint",
    "wait_for": "Waiting Time",
    "is_active": "Active",
    "threshold": "Threshold Bandwidth",
}


class SubscriptionCategory(enum.Enum):
    NODE_DOWN = "node_down"
    NODE_BANDWIDTH = "node_bandwidth"
    NODE_VERSION = "node_version"
    NODE_FLAG = "node_flag"


TABLE_HEADER_KEYS: dict[str, list[str]] = {
    SubscriptionCategory.NODE_DOWN.value: ["fingerprint", "wait_for", "is_active"],
    SubscriptionCategory.NODE_FLAG.value: ["fingerprint", "wait_for", "is_active"],
    SubscriptionCategory.NODE_VERSION.value: ["fingerprint", "wait_for", "is_active"],
    SubscriptionCategory.NODE_BANDWIDTH.value: [
        "fingerprint",
        "wait_for",
        "threshold",
        "is_active",
    ],
}

SubscriptionType: dict[str, SubscriptionTypeInterface] = {
    "node-down": {
        "internal_value": "node_down",
        "db_value": "node_down_sub",
        "display_name": "Node Down",
        "category": SubscriptionCategory.NODE_DOWN,
    },
    "node-bandwidth": {
        "internal_value": "node_bandwidth",
        "db_value": "node_bandwidth_sub",
        "display_name": "Node Bandwidth",
        "category": SubscriptionCategory.NODE_BANDWIDTH,
    },
    "node-version": {
        "internal_value": "node_version",
        "db_value": "node_version_sub",
        "display_name": "Node Version",
        "category": SubscriptionCategory.NODE_VERSION,
    },
    "node-flag-exit": {
        "internal_value": "node_flag_exit",
        "db_value": "node_flag_exit_sub",
        "display_name": "Node Flag Exit",
        "category": SubscriptionCategory.NODE_FLAG,
    },
    "node-flag-fast": {
        "internal_value": "node_flag_fast",
        "db_value": "node_flag_fast_sub",
        "display_name": "Node Flag Fast",
        "category": SubscriptionCategory.NODE_FLAG,
    },
    "node-flag-guard": {
        "internal_value": "node_flag_guard",
        "db_value": "node_flag_guard_sub",
        "display_name": "Node Flag Guard",
        "category": SubscriptionCategory.NODE_FLAG,
    },
    "node-flag-stable": {
        "internal_value": "node_flag_stable",
        "db_value": "node_flag_stable_sub",
        "display_name": "Node Flag Stable",
        "category": SubscriptionCategory.NODE_FLAG,
    },
    "node-flag-valid": {
        "internal_value": "node_flag_valid",
        "db_value": "node_flag_valid_sub",
        "display_name": "Node Flag Valid",
        "category": SubscriptionCategory.NODE_FLAG,
    },
}


class MessageResponseTypes(enum.Enum):
    SUCCESS = "success"
    WARNING = "warning"
    ERROR = "error"


class ResponseCode(enum.Enum):
    SUCCESS = "200"
    REDIRECT = "302"
    BAD_REQUEST = "400"
    UNAUTHORIZED = "401"
    NOT_FOUND = "404"
    SERVER_ERROR = "500"


class SnackbarMessageMapping(enum.Enum):
    MSG01 = {
        "message": "Account was successfully verified. Please proceed to login.",
        "category": MessageResponseTypes.SUCCESS.value,
    }
    MSG02 = {
        "message": "User was already verified. You can proceed to Login",
        "category": MessageResponseTypes.WARNING.value,
    }
    MSG03 = {
        "message": "User with this email already exists! Please login instead.",
        "category": MessageResponseTypes.WARNING.value,
    }
    MSG04 = {
        "message": "A verification email has been sent. Click on the link sent to continue.",
        "category": MessageResponseTypes.SUCCESS.value,
    }
    MSG05 = {
        "message": """Your email verification is still pending. We have sent a new email,
                             please click on the link to activate your account.""",
        "category": MessageResponseTypes.WARNING.value,
    }
    MSG06 = {
        "message": "There is no user with this email id. Please register for a new account.",
        "category": MessageResponseTypes.ERROR.value,
    }
    MSG07 = {
        "message": "Incorrect username, password combination",
        "category": MessageResponseTypes.ERROR.value,
    }
    MSG08 = {
        "message": "Subscription successfully created!",
        "category": MessageResponseTypes.SUCCESS.value,
    }
    MSG09 = {
        "message": "Subscription successfully modified!",
        "category": MessageResponseTypes.SUCCESS.value,
    }
    MSG10 = {
        "message": "Subscription successfully deleted!",
        "category": MessageResponseTypes.SUCCESS.value,
    }
    MSG11 = {
        "message": "Subscription successfully disabled!",
        "category": MessageResponseTypes.SUCCESS.value,
    }
    MSG12 = {
        "message": "Subscription successfully enabled!",
        "category": MessageResponseTypes.SUCCESS.value,
    }
    MSG13 = {
        "message": "Subscription does not exist. Create one instead!",
        "category": MessageResponseTypes.ERROR.value,
    }


class SnackbarMessage(enum.Enum):
    EMAIL_VERIFICATION_SUCCESS = SnackbarMessageMapping.MSG01.value
    EMAIL_VERIFICATION_REPEAT = SnackbarMessageMapping.MSG02.value
    USER_ALREADY_EXISTS = SnackbarMessageMapping.MSG03.value
    VERIFICATION_EMAIL_SENT = SnackbarMessageMapping.MSG04.value
    VERIFICATION_EMAIL_RESEND = SnackbarMessageMapping.MSG05.value
    USER_NOT_FOUND = SnackbarMessageMapping.MSG06.value
    INCORRECT_CREDENTIALS = SnackbarMessageMapping.MSG07.value
    SUBSCRIPTION_CREATED = SnackbarMessageMapping.MSG08.value
    SUBSCRIPTION_MODIFIED = SnackbarMessageMapping.MSG09.value
    SUBSCRIPTION_DELETED = SnackbarMessageMapping.MSG10.value
    SUBSCRIPTION_DISABLED = SnackbarMessageMapping.MSG11.value
    SUBSCRIPTION_ENABLED = SnackbarMessageMapping.MSG12.value
    SUBSCRIPTION_DOES_NOT_EXIST = SnackbarMessageMapping.MSG13.value


TEMPLATE_NS_DISC: str = (
    "External Routes containing templates to be called by the end-users"
)
EXTERNAL_NS_DISC: str = "External Routes for form actions to be called by the end-users"
