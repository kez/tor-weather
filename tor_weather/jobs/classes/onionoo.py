from typing import List

from tor_weather.env import get_env
from tor_weather.models.details_payload import (
    DetailsPayloadInterface,
    RelayPayloadInterface,
)
from tor_weather.utilities import http_request


class Onionoo:
    """Class representing the Onionoo Response"""

    def __init__(self) -> None:
        pass

    def _replace_reserved_keyword_keys(self, data: dict) -> DetailsPayloadInterface:
        """Replaces the keys with reserved keynames with alternate keynames

        Args:
            data (dict): The initial API Payload

        Returns:
            DetailsPayloadInterface: API Payload with reserved keynames replaced
        """
        relay_data = data.get("relays")
        if relay_data:
            for relay in relay_data:
                relay["relayAs"] = relay.pop("as")
                if relay.get("asName"):
                    relay["relayAsName"] = relay.pop("asName")
            return data  # type: ignore
        return None  # type: ignore

    def _get_relays(self, data: DetailsPayloadInterface) -> List[RelayPayloadInterface]:
        """Gets the relays from the API Payload

        Args:
            data (dict): Massaged API Payload

        Returns:
            List[RelayPayloadInterface]: List of Relays
        """
        return data.get("relays")  # type: ignore

    def get_relays(self) -> List[RelayPayloadInterface]:
        """Fetches the list of relays from the Onionoo API"""
        endpoint = f"{get_env('API_URL')}/details"
        response: dict = http_request(endpoint)
        translated_response = self._replace_reserved_keyword_keys(response)
        return self._get_relays(translated_response)
