from datetime import datetime

from tor_weather.core.database import Relay
from tor_weather.extensions import db, job_logger
from tor_weather.models.details_payload import RelayPayloadInterface


class RelayObject:
    """Object Representing a Relay"""

    def __init__(self, relay: RelayPayloadInterface):
        self.fingerprint = relay.get("fingerprint")
        self.version = self._get_version_details(relay)
        self.date = self._get_first_and_last_seen_details(relay)
        self.flag = self._get_flag_details(relay)
        self.bandwidth = self._get_bandwidth_details(relay)
        self.running = relay.get("running")

    def __repr__(self) -> str:
        return f"<RelayClass: fingerprint: {self.fingerprint}, running: {self.running}>"

    def _get_database_instance(self) -> Relay:
        """
        Get instance of relay from the database
        """
        db_relay: Relay = Relay.query.filter_by(fingerprint=self.fingerprint).first()
        return db_relay

    def _parse_date(self, date: str) -> datetime:
        """Parses the date into datetime object.

        Args:
            date (str): Datetime in string format

        Returns:
            datetime: Datetime in datetime format
        """
        return datetime.strptime(date, "%Y-%m-%d %H:%M:%S")

    def _get_version_details(self, relay: RelayPayloadInterface):
        """Parses the version information from the relay data

        Args:
            relay (_type_): API relay object

        Returns:
            _type_: Parsed relay object
        """
        return {
            "id": relay.get("version"),
            "status": relay.get("versionStatus"),
            "recommended": relay.get("recommendedVersion"),
            "platform": relay.get("platform"),
        }

    def _get_bandwidth_details(self, relay: RelayPayloadInterface) -> dict[str, int]:
        """Parses the bandwidth information from the relay data

        Args:
            relay (RelayPayloadInterface): API Relay object

        Returns:
            _type_: Parsed bandwidth object
        """
        return {
            "observed": relay.get("observedBandwidth"),
            "advertised": relay.get("advertisedBandwidth"),
            "burst": relay.get("bandwidthBurst"),
            "rate": relay.get("bandwidthRate"),
        }

    def _get_first_and_last_seen_details(
        self, relay: RelayPayloadInterface
    ) -> dict[str, datetime]:
        """Parses the first & last seen information from the relay data

        Args:
            relay (_type_): API relay object

        Returns:
            _type_: Parsed first & last seen object
        """
        return {
            "first_seen": self._parse_date(relay["firstSeen"]),
            "last_seen": self._parse_date(relay["lastSeen"]),
        }

    def _get_flag_details(self, relay: RelayPayloadInterface) -> dict[str, bool]:
        """Parses the flags information from the relay data

        Args:
            relay (_type_): API relay object

        Returns:
            _type_: Parsed flags object
        """
        return {
            "exit": "Exit" in relay["flags"],
            "guard": "Guard" in relay["flags"],
            "running": "Running" in relay["flags"],
            "stable": "Stable" in relay["flags"],
            "fast": "Fast" in relay["flags"],
            "valid": "Valid" in relay["flags"],
        }

    def _add_relay_to_db(self) -> None:
        """
        Add a relay to the database
        """
        relay: Relay = Relay(
            fingerprint=self.fingerprint,
            is_exit=self.flag["exit"],
            first_seen=self.date["first_seen"],
            last_seen=self.date["last_seen"],
            version=self.version["id"],
            recommended_version=self.version["recommended"],
            is_up=self.running,
        )
        job_logger.info(f"Adding relay in database - {self.fingerprint}")
        db.session.add(relay)
        db.session.commit()

    def get_database_obj(self) -> Relay:
        """
        Get the database object for the relay
        """
        db_relay: Relay = self._get_database_instance()
        if db_relay:
            job_logger.info(f"Relay found in database - {self.fingerprint}")
            return db_relay
        else:
            job_logger.info(f"Relay was not found in database - {self.fingerprint}")
            self._add_relay_to_db()
            return self._get_database_instance()
