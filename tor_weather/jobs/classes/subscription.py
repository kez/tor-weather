from typing import Any

from tor_weather.core.database.tables.subscriber import Subscriber
from tor_weather.core.database.tables.subscription import Subscription
from tor_weather.extensions import db


class SubscriptionObject:
    """Object Representing a Subscription"""

    def __init__(self, subscription: Subscription) -> None:
        self.subscription: Subscription = subscription
        self.child_sub = None

    def get_subscriber(self) -> Subscriber:
        """
        Get the subscriber for the subscription
        """
        subscriber: Subscriber = self.subscription.subscriber
        return subscriber

    def parse(self, child_sub_key: str):
        """
        Parse the subscription object to extract the child subscription
        """
        self.child_sub = getattr(self.subscription, child_sub_key)

    # This should expose method for accessing values for the subscription
    def get_value(self, key: str) -> Any:
        """
        Get value of property from the subscription
        """
        return getattr(self.child_sub, key)

    # This should expose methods for modifying a key-value pair
    def set_value(self, key: str, value: Any) -> None:
        """
        Set value of property in the subscription
        """
        setattr(self.child_sub, key, value)

    def commit(self) -> None:
        """
        Commit changes on the subscription
        """
        db.session.add(self.child_sub)
        db.session.commit()
