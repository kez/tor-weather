from datetime import datetime
from typing import Any, Type

from tor_weather.core.mail import NodeFlagEmail
from tor_weather.extensions import job_logger
from tor_weather.jobs.classes.relay import RelayObject
from tor_weather.jobs.classes.subscription import SubscriptionObject
from tor_weather.jobs.subscriptions.subscription_job import SubscriptionJob
from tor_weather.utilities import time_diff


class NodeFlagSubscriptionJob(SubscriptionJob):
    def __init__(
        self, flag: str, subscription_obj: SubscriptionObject, relay_obj: RelayObject
    ) -> None:
        super().__init__(subscription_obj, relay_obj)
        self.email_class: Type[NodeFlagEmail] = NodeFlagEmail  # type: ignore
        self.child_sub_name: str = f"node_flag_{flag}_sub"
        self.set_child_subscription(self.child_sub_name)

    def get_data_for_mail(self) -> Any:
        return {
            "fingerprint": self.relay.fingerprint,
            "wait_for": self.subscription.get_value("wait_for"),
            "flag": self.flag,
        }

    def validate(self) -> None:
        job_logger.info(
            f"[Node Flag] [{self.flag}] Validating - {self.relay.fingerprint}"
        )
        if self.subscription.child_sub:
            job_logger.info(
                f"[Node Flag] [{self.flag}] Found Subscription - {self.relay.fingerprint}"
            )
            if not self.subscription.get_value("issue_first_seen"):
                # Relay previously had the stable flag
                if not self.relay.flag[self.flag]:  # type: ignore
                    # Relay was running fine & has just lost the stable flag
                    job_logger.info(
                        f"[Node Flag] [{self.flag}] Anomaly Detected - {self.relay.fingerprint}"
                    )
                    self.subscription.set_value("issue_first_seen", datetime.utcnow())
            else:
                # Relay previously had the stable flag missing
                if self.relay.flag[self.flag]:  # type: ignore
                    # Relay was previously missing the stable flag and just got it back
                    job_logger.info(
                        f"[Node Flag] [{self.flag}] Fix Detected - {self.relay.fingerprint}"
                    )
                    self.subscription.set_value("issue_first_seen", None)
                    self.subscription.set_value("emailed", False)
                else:
                    # Relay previously had the stable flag missing & is still missing it
                    job_logger.info(
                        f"[Node Flag] [{self.flag}] Continued Downtime - {self.relay.fingerprint}"
                    )
                    issue_first_seen = self.subscription.get_value("issue_first_seen")
                    wait_for = self.subscription.get_value("wait_for")
                    if time_diff(issue_first_seen, datetime.utcnow()) >= wait_for:
                        # Relay has been missing the stable flag for more than the waiting time
                        job_logger.info(
                            f"[Node Flag] [{self.flag}] Exceeded Threshold - {self.relay.fingerprint}"
                        )
                        if not self.subscription.get_value("emailed"):
                            # Subscriber was not sent an email already
                            self.subscription.set_value("emailed", True)
                            if self.subscription.get_value("is_active"):
                                # The subscription is active, we should send an email
                                mail_data = self.get_data_for_mail()
                                self.send_email(mail_data)
            self.subscription.commit()
