from typing import List

from tor_weather.core.database.tables.relay import Relay
from tor_weather.core.database.tables.subscription import Subscription
from tor_weather.extensions import job_logger
from tor_weather.jobs.classes.onionoo import Onionoo
from tor_weather.jobs.classes.subscription import SubscriptionObject
from tor_weather.jobs.subscriptions.node_bandwidth_job import (
    NodeBandwidthSubscriptionJob,
)
from tor_weather.jobs.subscriptions.node_flag_job import NodeFlagSubscriptionJob
from tor_weather.models.details_payload import RelayPayloadInterface

from .classes.relay import RelayObject
from .subscriptions.node_down_job import NodeDownSubscriptionJob


def onionoo_job() -> None:
    print("Onionoo Job ran!")
    try:
        # Fetch the data from the Onionoo API
        relays: List[RelayPayloadInterface] = Onionoo().get_relays()
        # Loop over each relay fetched from the API
        for relay in relays:
            # Get the database object for the relay
            payload_relay: RelayObject = RelayObject(relay)
            db_relay: Relay = payload_relay.get_database_obj()
            # Get a list of subscriptions for the given relay
            subscriptions: List[Subscription] = db_relay.subscriptions
            # Validate all the subscriptions for the relay
            for subscription in subscriptions:
                subscription_obj: SubscriptionObject = SubscriptionObject(subscription)
                validate_subscription(subscription_obj, payload_relay)
        job_logger.info("Onionoo Job Completed")
    except Exception as e:
        job_logger.info(f"Onionoo Job Crashed - {e}")


def validate_subscription(subscription: SubscriptionObject, relay: RelayObject) -> None:
    """Returns the type of subscription from the database subscription object

    Args:
        subscription (_type_): Database subscription object

    Returns:
        _type_: Name of the subscription, Class for the subscription
    """
    job_logger.info(f"Validating Subscription for Relay - {relay.fingerprint}")
    # Validate Node-Status Subscriptions
    NodeDownSubscriptionJob(subscription, relay).validate()
    NodeBandwidthSubscriptionJob(subscription, relay).validate()
    # Validate Node-Flag Subscriptions
    NodeFlagSubscriptionJob("exit", subscription, relay).validate()
    NodeFlagSubscriptionJob("fast", subscription, relay).validate()
    NodeFlagSubscriptionJob("guard", subscription, relay).validate()
    NodeFlagSubscriptionJob("stable", subscription, relay).validate()
    NodeFlagSubscriptionJob("valid", subscription, relay).validate()
