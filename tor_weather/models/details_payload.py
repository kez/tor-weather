from typing import List, TypedDict


class RelayPayloadInterface(TypedDict):
    nickname: str
    fingerprint: str
    orAddresses: List[str]
    lastSeen: str
    lastChangedAddressOrPort: str
    firstSeen: str
    running: bool
    flags: List[str]
    country: str
    countryName: str
    relayAs: str
    relayAsName: str
    consensusWeight: int
    verifiedHostNames: List[str]
    lastRestarted: str
    bandwidthRate: int
    bandwidthBurst: int
    observedBandwidth: int
    advertisedBandwidth: int
    exitPolicy: List[str]
    # exitPolicySummary Left
    contact: str
    platform: str
    version: str
    versionStatus: str
    effectiveFamily: List[str]
    consensusWeightFraction: int
    guardProbability: int
    middleProbability: int
    exitProbability: int
    recommendedVersion: bool
    measured: bool


class DetailsPayloadInterface:
    version: str
    buildRevision: str
    relaysPublished: str
    relays: List[RelayPayloadInterface]
    relaysTruncated: int
    bridgesPublished: str
    bridges: List
    bridgesTruncated: int
