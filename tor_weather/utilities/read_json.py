import json


def read_json(file_name: str):
    """Reads the JSON from a file & returns it.

    Args:
        fileName (str): The relative path to the JSON File

    Returns:
        _type_: JSON Object
    """
    # dirPath = os.path.dirname(__file__)
    # filePath = os.path.join(os.path.dirname(__file__), fileName)
    f = open(file_name)
    data = json.load(f)
    f.close()
    return data
