import requests

from .camel_case_recursive import camel_case_recursive


def http_request(endpoint: str):
    response = requests.get(endpoint).json()
    response = camel_case_recursive(response)
    return response
