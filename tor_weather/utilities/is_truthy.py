from typing import Union, cast


def is_truthy(value: Union[bool, str]) -> bool:
    """Check if a value is truthy

    Args:
        value (str): Value to be checked

    Returns:
        bool: If the value is truthy
    """
    if type(value) is str:
        return value.lower() in ["true", "yes", "1"]
    else:
        return cast(bool, value)
